﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using Assets.PMesh.Unity;
using Assets.PMesh.Data.Outlines;
using Assets.PMesh.Data.Patterns;

namespace Assets.PMesh.Data.Patterns {
	/// <summary>
	/// Base class for all patterns. T refers to the type of outline this can be applied to
	/// </summary>
	public abstract class Pattern<T> where T : PatternParameters {
		//public abstract Dictionary<int, string> Cases { get; set; } //Pattern subdivided into cases. This allows some parameter values to be more likely than others. Each case given a name for ease of use

		//public abstract PatternParameters GenerateParameters (int case)

		public abstract void ApplyPattern (OutlineCollection outlineCollection, T parameters);
	}
}
