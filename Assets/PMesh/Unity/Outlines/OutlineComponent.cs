﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.PMesh.Data.Outlines;

namespace Assets.PMesh.Unity {
	/// <summary>
	/// Base class for Outline Components. Remember to override OnEnable method to allow OutlineCollectionComponent to detect that it has been added
	/// </summary>
	[RequireComponent(typeof(OutlineCollectionComponent))]
	public abstract class OutlineComponent : MonoBehaviour {
		public virtual Outline Outline { get; set; }

		public virtual void OnValidate () {
			GetComponent<OutlineCollectionComponent> ().Refresh ();
		}
	}
}