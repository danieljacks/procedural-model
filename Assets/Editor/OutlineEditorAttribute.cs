﻿using Assets.PMesh.Data.Outlines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Editor {
	/// <summary>
	/// Attribute specifying that a class is an editor of a specified outline type. Must also inherit from OutlineEditor
	/// </summary>
	[System.AttributeUsage (AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	sealed class OutlineEditorAttribute : System.Attribute {
		readonly Type _outlineType;

		public OutlineEditorAttribute (Type outlineType) {
			_outlineType = outlineType;
		}

		public Type OutlineType {
			get { return _outlineType; }
		}

		public bool IsValid () {
			var temp = typeof(Outline).IsAssignableFrom (OutlineType);
			return temp;
		}
	}
}