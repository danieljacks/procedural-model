﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using MeshDecimator.Unity;
using nobnak.Geometry;

[RequireComponent(typeof(MeshFilter))]
public class DecimatorTest : MonoBehaviour {

	private void OnGUI () {
		//if (GUI.Button(new Rect (10,10,100,20), "DECIMATE")) {
		//	Decimate ();
		//}
	}

	public void Decimate () {
		/*var meshF = GetComponent<MeshFilter> ();
		var simp = new Simplification (meshF.mesh.vertices, meshF.mesh.triangles);

		Vector3[] outVerts;
		int[] outTris;

		simp.CollapseEdge (simp.costs.RemoveFront ());

		simp.ToMesh (out outVerts, out outTris);
		meshF.mesh.vertices = outVerts;
		meshF.mesh.triangles = outTris;*/

		GetComponent<MeshFilter> ().sharedMesh = MeshDecimatorUtility.DecimateMesh (GetComponent<MeshFilter> ().sharedMesh, Matrix4x4.identity, 0.1f, false, null, true);
	}

	public void DecimateNoDistort () {

	}
}
