﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.PMesh.Data.Outlines;
using Assets.PMesh.Data.Patterns.Parameters;
using UnityEngine;

namespace Assets.PMesh.Data.Patterns {
	/// <summary>
	/// Subdivides all OutlineQuads in the supplied collection into the specified number of subdivisions
	/// </summary>
	public class SubdivideQuads : Pattern<SubdivideQuadParameters> {
		public override void ApplyPattern (OutlineCollection outlineCollection, SubdivideQuadParameters parameters) {
			var newQuads = new List<OutlineQuad> ();
			int numberOfQuadsX = parameters.DivisionsX + 1;
			int numberOfQuadsY = parameters.DivisionsY + 1;

			float distBetweenQuadsX = 1f / (float) numberOfQuadsX; //In UV coords
			float distBetweenQuadsY = 1f / (float) numberOfQuadsY;

			foreach (OutlineQuad quad in outlineCollection.FilterOutlines<OutlineQuad> ()) {
				Vector3 originalQuadOffset = quad.Offset;

				//This is basically just a grid contained in the quads. Each gridpoint is the origin of its own quad (no shared points)
				List<OutlineQuad> quads = new List<OutlineQuad> ();

				for (int y = 0; y < numberOfQuadsY; y++) {
					for (int x = 0; x < numberOfQuadsX; x++) {
						Vector3 botLeft = new Vector3 ( (x + 0) * distBetweenQuadsX, (y + 0) * distBetweenQuadsY);
						Vector3 botRight = new Vector3( (x + 1) * distBetweenQuadsX, (y + 0) * distBetweenQuadsY);
						Vector3 topRight = new Vector3( (x + 1) * distBetweenQuadsX, (y + 1) * distBetweenQuadsY);
						Vector3 topLeft = new Vector3 ( (x + 0) * distBetweenQuadsX, (y + 1) * distBetweenQuadsY);

						Vector3 a = botLeft;
						Vector3 b = botRight;
						Vector3 c = topRight;
						Vector3 d = topLeft;

						Vector3 ab = b - a; //Position vectors relative to botLeft (that's considered the origin)
						Vector3 ac = c - a;
						Vector3 ad = d - a; 
						
						OutlineQuad smallQuad = new OutlineQuad (Vector3.zero, ab, ac, ad);
						smallQuad.Offset = originalQuadOffset + botLeft;

						outlineCollection.Outlines.Add (smallQuad); //And add the small quad
					}
				}
			}

		}
	}
}
