﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[ExecuteInEditMode]
/// <summary>
/// Draws a gizmo for each vertex in mesh, highlighting overlapping meshes
/// </summary>
public class DrawVertices : MonoBehaviour {

	public Color defaultColor = Color.black;
	public Color doubledColor = Color.red;
	public float radius = 0.02f;

	private Dictionary<Vector3, Color> gizmoDictionary;

	private void OnEnable () {
		gizmoDictionary = new Dictionary<Vector3, Color> ();
		foreach (Vector3 vertex in GetComponent<MeshFilter> ().sharedMesh.vertices) {
			if (gizmoDictionary.ContainsKey(vertex)) break;

			bool doubled = false;
			foreach (Vector3 vert2 in GetComponent<MeshFilter> ().sharedMesh.vertices) {
				if (vertex == vert2) doubled = true;
			}

			if (doubled == true) {
				gizmoDictionary.Add (vertex, doubledColor);
			} else {
				gizmoDictionary.Add (vertex, defaultColor);
			}
		}
	}

	public void OnDrawGizmos () {
		/*foreach (Vector3 vertex in GetComponent<MeshFilter> ().sharedMesh.vertices) {
			Color color;
			if (gizmoDictionary.TryGetValue (vertex, out color)) {
				Gizmos.color = color;
				Gizmos.DrawSphere (transform.TransformPoint (vertex), radius);
			}
		}*/

		foreach (Vector3 vertex in GetComponent<MeshFilter>().sharedMesh.vertices) {
			Gizmos.color = defaultColor;
			Gizmos.DrawSphere (transform.TransformPoint (vertex), radius);
		}
	}
}
