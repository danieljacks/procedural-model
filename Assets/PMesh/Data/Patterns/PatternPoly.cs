﻿using Assets.PMesh.Data.Outlines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.PMesh.Data.Patterns {
	/// <summary>
	/// Base class for pattern that can be applied to an OutlinePoly
	/// </summary>
	//public abstract class PatternPoly : Pattern<OutlinePoly> {
	//	public abstract void ApplyPattern (OutlinePoly poly);
	//}
}