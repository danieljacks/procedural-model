﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVTest : MonoBehaviour {

	public float gizmoRadius = 0.01f;
	public Vector2 UV = Vector2.zero;
	public Vector3 BotLeft = new Vector3 (0, 0);
	public Vector3 BotRight = new Vector3 (1, 0);
	public Vector3 TopRight = new Vector3 (1, 1);
	public Vector3 TopLeft = new Vector3 (0, 1);

	[ExecuteInEditMode]
	private void OnDrawGizmos () {
		Gizmos.color = Color.black;
		Gizmos.DrawSphere (BotLeft, gizmoRadius);
		Gizmos.DrawSphere (BotRight, gizmoRadius);
		Gizmos.DrawSphere (TopLeft, gizmoRadius);
		Gizmos.DrawSphere (TopRight, gizmoRadius);

		Gizmos.color = Color.red;
		Gizmos.DrawSphere (UVToXY (UV), gizmoRadius);
	}

	public Vector2 UVToXY (Vector2 UVCoords) {
		float u = UVCoords.x;
		float v = UVCoords.y;

		Vector3 a = BotLeft;
		Vector3 b = BotRight;
		Vector3 c = TopRight;
		Vector3 d = TopLeft;

		Vector3 ab = Vector3.LerpUnclamped (a, b, u);
		Vector3 dc = Vector3.LerpUnclamped (d, c, u);

		Vector3 p = Vector3.LerpUnclamped (ab, dc, v);

		return p;
	}
}
