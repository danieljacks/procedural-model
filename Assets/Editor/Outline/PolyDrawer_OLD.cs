﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

using Assets.PMesh.Data.Outlines;

namespace Assets.Editor {
	/// <summary>
	/// Handles drawing of OutlinePoly
	/// </summary>
	class PolyDrawer_OLD {
		public Color OutlineColor { get; set; }
		public Color FillColor { get; set; }
		public float Alpha { get; set; }

		/// <summary>
		/// Use with DrawPoly() function.
		/// </summary>
		public PolyDrawer_OLD (Color outlineColor, Color fillColor, float alpha) {
			OutlineColor = outlineColor;
			FillColor = fillColor;
			Alpha = alpha;
		}

		public void DrawPoly (OutlinePoly poly, Transform parentTransform) {
			//DrawPoly (poly, Color.green, Color.blue, 0.2f);
			DrawPoly (poly, parentTransform, OutlineColor, FillColor, Alpha);
		}

		/// <summary>
		/// Draw the specified poly at parentTransform's position
		/// </summary>
		private void DrawPoly (OutlinePoly poly, Transform parentTransform, Color outlineColor, Color fillColor, float alpha) {
			Color originalColor = Handles.color;

			//Draw all edges in poly
			Handles.color = outlineColor;
			foreach (OutlineEdge edge in poly.GetAllEdges()) {
				Vector3 start = parentTransform.transform.TransformPoint (edge.StartPos);
				Vector3 end = parentTransform.transform.TransformPoint (edge.EndPos);
				Handles.DrawLine (start, end);
			}

			//Draw poly fill
			Handles.color = new Color (fillColor.r, fillColor.g, fillColor.b, alpha);
			Handles.DrawAAConvexPolygon (poly.Nodes.Select (n => parentTransform.transform.TransformPoint (n.Position)).ToArray ()); //Also transforms to world space

			//Reset handle color
			Handles.color = originalColor;
		}
	}
}
