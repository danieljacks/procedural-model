﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.PMesh.Data.Patterns.Parameters {
	public class SubdivideQuadParameters : PatternParameters {
		public int DivisionsX { get; set; }
		public int DivisionsY { get; set; }

		public static int MaxDivisionsX { get { return 6; } }
		public static int MaxDivisionsY { get { return 6; } }

		public SubdivideQuadParameters (int divisionsX, int divisionsY) {
			DivisionsX = divisionsX;
			DivisionsY = divisionsY;
		}

		public SubdivideQuadParameters GenerateRandom () {
			int x = UnityEngine.Random.Range (0, MaxDivisionsX + 1);
			int y = UnityEngine.Random.Range (0, MaxDivisionsY + 1);

			return new SubdivideQuadParameters (x, y);
		}
	}
}
