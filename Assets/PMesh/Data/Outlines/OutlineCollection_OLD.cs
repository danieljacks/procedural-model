﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Assets.PMesh.Data.Outlines {
	/// <summary>
	/// A collection of Outline elements. Once patterns are applied, single outline elements are often broken down. This class retains the user-defined structure, while preserving pattern effects.
	/// This class automatically handles rerouting of outline elements to the correct subclass list
	/// </summary>
	public class OutlineCollection_OLD {
		//GUIDELINES FOR MODIFICATION:
		//When adding subclasses, add subclasses to all get methods of parent classes.
		//When adding parent classes, add all subclasses in get method

		#region fields
		private List<OutlinePoly> _outlinePolys = new List<OutlinePoly> ();
#endregion

		#region Properties
		/// <summary>
		/// Offset of entire outlineCollection group
		/// </summary>
		public Vector3 Offset { get; set; }

		/// <summary>
		/// Includes all poly subclasses
		/// </summary>
		public List<OutlinePoly> OutlinePolys {
			get {
				var polysAndChildren = new List<OutlinePoly> ();
				polysAndChildren.AddRange (OutlineQuads.Select (poly => (OutlinePoly) poly)); //Adds outlineQuads and converts them to outlinePolys
				return polysAndChildren;
			}
			set {
				_outlinePolys = value;
				ReroutePolySubclasses ();
			}
		}
		public List<OutlineLine> OutlineLines { get; set; }
		public List<OutlineQuad> OutlineQuads { get; set; }
#endregion

		#region Constructors
		public OutlineCollection_OLD (List<OutlinePoly> outlinePolys, List<OutlineLine> outlineLines, Vector3 offset) {
			OutlinePolys = outlinePolys;
			OutlineLines = outlineLines;
			Offset = offset;
		}

		public OutlineCollection_OLD (List<OutlinePoly> outlinePolys, List<OutlineLine> outlineLines) : this (outlinePolys, outlineLines, Vector3.zero){ }
		#endregion

		#region PrivateMethods
		private void ReroutePolySubclasses () {
			for (int i = OutlinePolys.Count - 1; i >= 0; i--) {
				//Switch element to type of subclass if corrent type for subclass
				if (OutlinePolys[i].GetType() == typeof (OutlineQuad)) {
					OutlineQuads.Add ((OutlineQuad) OutlinePolys[i]); 
					_outlinePolys.RemoveAt (i);
				}
			}
		}
#endregion
	}
}
