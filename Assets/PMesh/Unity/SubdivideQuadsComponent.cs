﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.PMesh.Data.Patterns;
using Assets.PMesh.Data.Patterns.Parameters;
using Assets.PMesh.Data.Outlines;


namespace Assets.PMesh.Unity {
	[RequireComponent (typeof (OutlineComponent))]
	public class SubdivideQuadsComponent : MonoBehaviour {

		public SubdivideQuads Pattern { get; set; }
		public SubdivideQuadParameters PatternParameters { get; set; }

		public void ApplyPattern () {
			
		}
	}
}