﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using Assets.PMesh.Data;
using Assets.PMesh.Unity;

namespace Assets.Editor {

	[CustomEditor (typeof (OutlinePolyComponent))]
	public class OutlinePolyComponentEditor : UnityEditor.Editor {

		private OutlinePolyComponent PolyComponent { get; set; }

		private HandleDrawer handleDrawer;
		private PolyDrawer_OLD polyDrawer;
		private OutlinePolyInspector inspectorDrawer;


		void OnEnable () {
			PolyComponent = (OutlinePolyComponent) target;

			handleDrawer = new HandleDrawer (0.06f, 0.08f);
			polyDrawer = new PolyDrawer_OLD (Color.green, Color.blue, 0.2f);
			inspectorDrawer = new OutlinePolyInspector ();
			
			SceneView.onSceneGUIDelegate -= DrawScene;
			SceneView.onSceneGUIDelegate += DrawScene;
		}

		private void OnDisable () {
			SceneView.onSceneGUIDelegate -= DrawScene;
		}

		public override void OnInspectorGUI () {
			inspectorDrawer.DrawInspector (PolyComponent);
		}

		public void DrawInspector () { }

		public void DrawScene (SceneView view) {
			handleDrawer.DrawHandles (PolyComponent.OutlinePoly, PolyComponent.transform );

			polyDrawer.DrawPoly (PolyComponent.OutlinePoly, PolyComponent.transform);
		}

	}
}