﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.PMesh.Data.Outlines;

namespace Assets.Editor {
	/// <summary>
	/// Parent class of Outline Editors. Must also decorate outline editor with OutlineEditorAttribute. Parameter of attribute determines the type of Outline that the editor accepts
	/// </summary>
	public class OutlineEditor : UnityEditor.Editor {

		public virtual void DrawInspector (Outline outline, Transform outlineTransform) {
			string displayString = "This the default outline editor. \nImplement a custom editor of type: <b>" + outline.GetType ().Name + "</b> \nto view proper inspector functionality";
			GUIStyle style = EditorStyles.helpBox;
			style.richText = true;
			
			EditorGUILayout.LabelField (displayString, style);
		}

		public virtual void DrawScene (Outline outline, Transform outlineTransform) { }
	}
}