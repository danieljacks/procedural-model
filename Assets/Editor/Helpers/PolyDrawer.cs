﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

using Assets.PMesh.Data.Outlines;

namespace Assets.Editor {
	/// <summary>
	/// Handles drawing of OutlinePoly
	/// </summary>
	public static class PolyDrawer {
		/// <summary>
		/// Draw the specified poly at parentTransform's position
		/// </summary>
		public static void DrawPoly (OutlinePoly poly, Transform parentTransform, Color outlineColor, Color fillColor, float alpha) {
			Color originalColor = Handles.color;

			//Draw all edges in poly
			Handles.color = outlineColor;
			foreach (OutlineEdge edge in poly.GetAllEdges ()) {
				Vector3 start = parentTransform.transform.TransformPoint (edge.StartPos);
				Vector3 end = parentTransform.transform.TransformPoint (edge.EndPos);
				Handles.DrawLine (start, end);
			}

			//Draw poly fill
			Handles.color = new Color (fillColor.r, fillColor.g, fillColor.b, alpha);
			Handles.DrawAAConvexPolygon (poly.Nodes.Select (n => parentTransform.transform.TransformPoint (n.Position)).ToArray ()); //Also transforms to world space

			//Reset handle color
			Handles.color = originalColor;
		}
	}
}
