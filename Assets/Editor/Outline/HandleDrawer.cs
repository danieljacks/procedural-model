﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.PMesh.Data.Outlines;

namespace Assets.Editor {
	/// <summary>
	/// Draws handles for given OutlinePoly
	/// </summary>
	public class HandleDrawer {
		public float DotSize { get; set; }
		public float DotPickSize { get; set; }

		private int SelectedNodeIndex { get; set; }

		/// <summary>
		/// Creates a new HandleDrawer. Activate through DrawHandles () method
		/// </summary>
		/// <param name="poly">Outline poly that the drawer should draw points for</param>
		/// <param name="parentTransform">Transform containing outline polys. Affects handle rotation and position</param>
		public HandleDrawer (float dotSize, float dotPickSize ) {
			DotSize = dotSize;
			DotPickSize = dotPickSize;
		}

		public void DrawHandles (OutlinePoly poly, Transform parentTransform) {
			//Create handles for all poly vertices in all polys
			for (int i = 0; i < poly.Nodes.Count; i++) {
				DrawHandle (poly, parentTransform, i, 0.06f, 0.08f);
			}
		}

		/// <summary>
		/// Draws handle for single node index
		/// </summary>
		private void DrawHandle (OutlinePoly poly, Transform parentTransform, int nodeIndex, float dotSize, float dotPickSize) {
			Quaternion handleRotation = GetHandleRotation (parentTransform);

			Vector3 initialWorldCoord = parentTransform.transform.TransformPoint (poly.GetNode(nodeIndex).Position);

			//Dots allow selection to draw movable handle
			if (DotButton (initialWorldCoord, handleRotation, dotSize, dotPickSize)) {
				SelectedNodeIndex = nodeIndex;
			}

			//Draw moveable handle if current index is selected
			if (nodeIndex == SelectedNodeIndex) {
				EditorGUI.BeginChangeCheck ();

				Vector3 movedNodeCoord = DoSelectedHandle (parentTransform, initialWorldCoord, handleRotation);

				if (EditorGUI.EndChangeCheck ()) {
					if (!EditorApplication.isPlaying) {//Can only do this in editmode
						Undo.RecordObject (parentTransform, "Move Point"); //Set undo
						EditorUtility.SetDirty (parentTransform);

						poly.Nodes[nodeIndex].Position = movedNodeCoord; //Modify poly node coord
					}
				}
			}
		}

		/// <summary>
		/// Draws selected handle and returns the handle's new position
		/// </summary>
		/// <returns></returns>
		private Vector3 DoSelectedHandle (Transform parentTransform, Vector3 initialPosition, Quaternion rotation) {
			return parentTransform.InverseTransformPoint (Handles.DoPositionHandle (initialPosition, rotation));
		}

		/// <summary>
		/// Draws a dot button and returns true if it has been selected
		/// </summary>
		private bool DotButton (Vector3 position, Quaternion rotation, float dotSize, float dotPickSize) {
			float size = HandleUtility.GetHandleSize (position);

			if (Handles.Button (position, rotation, size * dotSize, size * dotPickSize, Handles.DotHandleCap)) {
				return true;
			}
			else {
				return false;
			}
		}

		/// <summary>
		/// Returns Quaternion of handle's rotation, which is aligned with parent transform's rotation and complies with pivot settings
		/// </summary>
		private Quaternion GetHandleRotation (Transform parentTransform) {
			Quaternion handleRotation = Tools.pivotRotation == PivotRotation.Local ? //Comply with local or global handle settings
				parentTransform.rotation : Quaternion.identity;

			return handleRotation;
		}

	}
}