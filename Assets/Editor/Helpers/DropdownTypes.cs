﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace Assets.Editor {
	public class DropdownGenerated : PopupWindowContent {
		private string[] strings;
		private int selectedIndex;

		public override Vector2 GetWindowSize () {
			return new Vector2 (200, 150);
		}

		public DropdownGenerated (Func<string[]> getStrings, int selectedIndex) {
			strings = getStrings ();
			this.selectedIndex = selectedIndex;
		}

		public override void OnGUI (Rect rect) {
			
		}
	}
}