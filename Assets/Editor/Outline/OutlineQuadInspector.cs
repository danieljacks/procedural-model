﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.PMesh.Unity;
using Assets.PMesh.Data.Outlines;
using UnityEditor;

namespace Assets.Editor {
	/// <summary>
	/// Handles drawing of OutlineQuad inspector
	/// </summary>
	public class OutlineQuadInspector {
		private bool showNodes = false;

		public void DrawInspector (OutlineQuadComponent QuadComponent) {
			OutlineQuad quad = QuadComponent.OutlineQuad;

			DrawNodeField (QuadComponent, quad, 0, "BotLeft: ");
			DrawNodeField (QuadComponent, quad, 1, "BotRight: ");
			DrawNodeField (QuadComponent, quad, 2, "TopRight: ");
			DrawNodeField (QuadComponent, quad, 3, "TopLeft: ");

		}

		private void DrawNodeField (OutlineQuadComponent QuadComponent, OutlineQuad quad, int i, string label) {
			EditorGUILayout.BeginHorizontal ();

			EditorGUI.BeginChangeCheck ();

			Vector3 modifiedPos = EditorGUILayout.Vector3Field (label, quad.Nodes[i].Position);

			if (EditorGUI.EndChangeCheck ()) { //Undo operations
				EditorUtility.SetDirty (QuadComponent);

				Undo.RegisterCompleteObjectUndo (QuadComponent, "Moved Node");
				QuadComponent.OutlineQuad.Nodes[i].Position = modifiedPos;

				SceneView.RepaintAll ();
			}

			EditorGUILayout.EndHorizontal ();
		}
	}
}