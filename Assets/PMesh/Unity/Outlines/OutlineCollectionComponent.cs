﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.PMesh.Data.Outlines;
using UnityEngine;

namespace Assets.PMesh.Unity {
	public class OutlineCollectionComponent : MonoBehaviour {
		private OutlineCollection _outlineCollection = new OutlineCollection ();
		/// <summary>
		/// Lazy instantiated
		/// </summary>
		public OutlineCollection OutlineCollection {
			get {
				if (_outlineCollection == null) _outlineCollection = new OutlineCollection ();
				return _outlineCollection;
			}
			set {
				_outlineCollection = value;
			}
		}

		public void OnValidate () {
			Refresh ();
		}

		/// <summary>
		/// Researches for OutlineComponents and updates internal list
		/// </summary>
		public void Refresh () {
			OutlineCollection.Outlines = GetComponents<OutlineComponent> ().Select (c => c.Outline).ToList ();
		}
	}
}
