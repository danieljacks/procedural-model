﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PMesh_old.Data {
	[System.Serializable]
	/// <summary>
	/// Data container for procedural outline lines
	/// </summary>
	public class OutlineLine {
		[SerializeField]
		private readonly OutlinePoly _parentPoly;
		/// <summary>
		/// Poly to which Start and End indices refer. Readonly
		/// </summary>
		public OutlinePoly ParentPoly {
			get {
				return _parentPoly;
			}
		}

		[SerializeField]
		private int _startPoint;
		/// <summary>
		/// Beginning of line segment as the index of the point in its containing poly
		/// </summary>
		public int StartPoint {
			get {
				return _startPoint;
			}
			set {
				_startPoint = value;
				if (value < 0) throw new System.ArgumentOutOfRangeException ();
			}
		}
		public Vector3 StartPointVector {
			get {
				if (StartPoint < ParentPoly.Points.Count)
					return ParentPoly.Points[StartPoint];
				else
					return Vector3.zero;
			}
		}

		[SerializeField]
		private int _endPoint;
		/// <summary>
		/// End of line segment as the index of the point in its containing poly
		/// </summary>
		public int EndPoint {
			get {
				return _endPoint;
			}
			set {
				_endPoint = value;
				if (value < 0) throw new System.ArgumentOutOfRangeException ();
			}
		}
		public Vector3 EndPointVector {
			get {
				if (EndPoint < ParentPoly.Points.Count)
					return ParentPoly.Points[EndPoint];
				else
					return Vector3.zero;
			}
		}

		[SerializeField]
		[Range(0.0f,1.0f)]
		private List<float> _handlePoints;
		/// <summary>
		/// 'Handles' used to attach things to line segment.
		/// Given as a percentage, i.e. linear interpolation from StartPoint to EndPoint.
		/// These can be automatically generated with a degree of randomness.
		/// Use GetMidPoint and SetMidPoint for access.
		/// Automatically sorted on each modification.
		/// </summary>
		public List<float> HandlePoints {
			get {
				return _handlePoints;
			}
			private set {
				_handlePoints = value;
			}
		}

		public OutlineLine (OutlinePoly parentPoly) {
			_parentPoly = parentPoly;
		}

		public OutlineLine (OutlinePoly parentPoly, int startPoint, int endPoint) : this (parentPoly){
			StartPoint = startPoint;
			EndPoint = endPoint;
		}

		public OutlineLine (OutlinePoly parentPoly, int startPoint, int endPoint, List<float> handlePoints) : this (parentPoly, startPoint, endPoint) {
			HandlePoints = handlePoints;
		}

		/// <summary>
		/// Generate OutlineLine with random HandlePoints
		/// </summary>
		/// <param name="startPoint">Start of line</param>
		/// <param name="endPoint">End of line</param>
		/// <param name="minHandlePoints">Minimum number of handle points to generate [inclusive]. Can be 0.</param>
		/// <param name="maxHandlePoints">Maximum number of handle points to generate [inclusive]. Can be the same as minHandlePoints</param>
		/// <param name="quasirandom">If false, true randomness will be used. Otherwise, a quasi-random distribution is used</param>
		/// <param name="randomness">Float in the 0..1 range determining how random the quasi-random distribution is, where 0 is completely uniform and 1 is the most random. Only used if quasirandom is set to true</param>
		public OutlineLine (OutlinePoly parentPoly, int startPoint, int endPoint, int minHandlePoints, int maxHandlePoints, bool quasirandom, float randomness) : this (parentPoly, startPoint, endPoint) {
			if (quasirandom == false) {
				ApplyRandomHandlePoints (minHandlePoints, maxHandlePoints);
			} else {
				ApplyQuasirandomHandlePoints (minHandlePoints, maxHandlePoints, randomness);
			}
		}

		/// <summary>
		/// Returns a Vector3 representation of the handle point with specified index.
		/// </summary>
		public Vector3 GetHandlePoint (int index) {
			if (index >= HandlePoints.Count || index < 0) throw new System.ArgumentOutOfRangeException ("HandlePoint index out of range");

			return Vector3.Lerp (ParentPoly.Points[StartPoint], ParentPoly.Points[EndPoint], HandlePoints[index]);
		}

		/// <summary>
		/// Returns a random point from the line.
		/// Can be the StartPoint, EndPoint, or a MidPoint
		/// </summary>
		public Vector3 GetRandomPoint () {
			int numberOfPoints = 1 + HandlePoints.Count; //0..mindpoint.count - 1 for handlepoints, numberofpoints - 1 for startpoint, numberofpoints for endpoint
			int rndInd = Random.Range (0, numberOfPoints + 1);

			if (rndInd == numberOfPoints - 1) {
				return ParentPoly.Points[StartPoint];
			} else if (rndInd == numberOfPoints) {
				return ParentPoly.Points[EndPoint];
			} else {
				return GetHandlePoint (rndInd);
			}
		}

		/// <summary>
		/// Randomly generates and applies HandlePoints with specified level of uniformity. Even if randomness is 1, points can only be generated within range of uniform positions. This means that there will never be all the points on 1 side.
		/// </summary>
		/// <param name="maxPoints">value of 1.0 indicates full randomness, value of 0.0 indicates complete uniformity</param>
		private void ApplyQuasirandomHandlePoints (int minPoints, int maxPoints, float randomness) {
			HandlePoints = GenerateQuasirandomHandlePoints (minPoints, maxPoints, randomness);

			SortHandlePoints ();
		}

		/// <summary>
		/// Generates and applies truly random handle points. They can all be on top of each other.
		/// </summary>
		private void ApplyRandomHandlePoints (int minPoints, int maxPoints) {
			HandlePoints = GenerateRandomHandlePoints (minPoints, maxPoints);

			SortHandlePoints ();
		}

		/// <summary>
		/// Randomly generates midpoints with specified level of uniformity. Even if randomness is 1, points can only be generated within range of uniform positions. This means that there will never be all the points on 1 side.
		/// </summary>
		/// <param name="maxPoints">value of 1.0 indicates full randomness, value of 0.0 indicates complete uniformity</param>
		public static List<float> GenerateQuasirandomHandlePoints (int minPoints, int maxPoints, float randomness) {
			if (minPoints < 0) throw new System.ArgumentOutOfRangeException ("minPoints < 0");
			if (maxPoints < minPoints) throw new System.ArgumentOutOfRangeException ("maxPoints < minPoints");
			if (maxPoints == 0) return new List<float> ();
			if (randomness < 0f || randomness > 1f) throw new System.ArgumentOutOfRangeException ("randomness must be between 0 and 1");

			int numberOfPoints = minPoints;
			if (minPoints != maxPoints) { //Dont generate number if min and max points the same
				numberOfPoints = Random.Range (minPoints, maxPoints + 1); //Upper bound not inclusive
			}

			float distBetweenPts = 1 / ((float) (numberOfPoints + 1)); //There are numberOfPoints + 1 line segments (i.e. distances between pts)
			float maxDeviation = distBetweenPts * randomness; //How far a point can move from uniform position

			List<float> handlePoints = new List<float> ();

			for (int i = 1; i <= numberOfPoints; i++) {
				float distributedPoint = (float) i * distBetweenPts; //i.e. not random pts
				float pt = distributedPoint + Random.value * maxDeviation;

				handlePoints.Add (pt); //Really random
			}

			return handlePoints;
		}

		/// <summary>
		/// Generates truly random handle points. They might all be on top of each other.
		/// </summary>
		public static List<float> GenerateRandomHandlePoints (int minPoints, int maxPoints) {
			if (minPoints < 0) throw new System.ArgumentOutOfRangeException ("minPoints < 0");
			if (maxPoints < minPoints) throw new System.ArgumentOutOfRangeException ("maxPoints < minPoints");
			if (maxPoints == 0) return new List<float> ();

			int numberOfPoints = minPoints;
			if (minPoints != maxPoints) { //Dont generate number if min and max points the same
				numberOfPoints = Random.Range (minPoints, maxPoints + 1); //Upper bound not inclusive
			}

			List<float> handlePoints = new List<float> ();

			for (int i = 1; i <= numberOfPoints; i++) {
				handlePoints.Add (Random.value); //Really random
			}

			return handlePoints;
		}

		/// <summary>
		/// Adds handle point.
		/// </summary>
		/// <param name="relativePos">Float in 0..1 range representing percentage of position from StartPoint to EndPoint of Handle</param>
		public void AddHandlePoint (float relativePosition) {
			HandlePoints.Add (relativePosition);
			SortHandlePoints ();
		}

		/// <summary>
		/// Removes HandlePoint at specified index. Shallow wrapper for List<> functionality
		/// </summary>
		/// <param name="handleIndex"></param>
		public void RemoveHandleAt (int handleIndex) {
			HandlePoints.RemoveAt (handleIndex);
		}

		/// <summary>
		/// Removes HandlePoint with specified value. Shallow wrapper for List<> functionality
		/// </summary>
		public void RemoveHandle (float handle) {
			HandlePoints.Remove (handle);
		}

		private void SortHandlePoints () {
			HandlePoints.Sort ();
		}
	}
}