﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using Assets.PMesh.Data.Outlines;
using System;

namespace Assets.Editor {
	[OutlineEditor (typeof (OutlineQuad))]
	public class OutlineQuadEditor : OutlineEditor {

		public override void DrawInspector (Outline outlineQuad, Transform quadTransform) {
			outlineQuad = (OutlineQuad) outlineQuad;

			EditorGUILayout.HelpBox ("THIS IS A QUAD INSPEKTORRR", MessageType.Info);
		}

		public override void DrawScene (Outline outlineQuad, Transform quadTransform) {
			//outlineQuad = (OutlineQuad) outlineQuad;

			PolyDrawer.DrawPoly ((OutlinePoly) outlineQuad, quadTransform, Color.black, Color.red, 0.2f);
		}
	}
}