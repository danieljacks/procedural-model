﻿using UnityEngine;

namespace Assets.PMesh.Data.Outlines {
	public struct OutlineEdge {
		public readonly Vector3 StartPos;
		public readonly Vector3 EndPos;

		public OutlineEdge (Vector3 startPos, Vector3 endPos) {
			StartPos = startPos;
			EndPos = endPos;
		}
	}
}
