﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class ArrayHelper {

	/// <summary>
	/// Fill enumerable with items
	/// </summary>
	/// <param name="enumerable"></param>
	/// <param name="numberOfItems"></param>
	/// <returns></returns>
	//static T EnumerableTestFill<T> (T enumerable, int numberOfItems, Func<int, T> fillFunc) where T : IEnumerable { 
	//	for (int i = 0; i <numberOfItems; i++) {
	//
	//	}
	//}

	public static void ForEach<T> (this IEnumerable<T> source, Action<T> action) {   // note: omitted arg/null checks
		foreach (T item in source) { action (item); }
	}
}
