﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PMesh_old.Data {
	[System.Serializable]
	/// <summary>
	/// Represents an outline polygon with an arbitrary number of connected lines. Set of points connected with OutlineLines.
	/// </summary>
	public class OutlinePoly {
		/// <summary>
		/// Default set of points that a poly is initialised with
		/// </summary>
		public static List<Vector3> DefaultPoints {
			get {
				return new List<Vector3> () {
					new Vector3 (0, 0, 0),
					new Vector3 (1, 0, 0),
					new Vector3 (1, 1, 0),
					new Vector3 (0, 1, 0)
				};
			}
		}

		[SerializeField]
		private List<OutlineLine> _outlineLines = new List<OutlineLine> ();
		/// <summary>
		/// List of OutlineLines making up the OutlinePoly. Can be set indirectly by modifying Points.
		/// There are the same number of lines as points. Indices of line based on start index of point
		/// </summary>
		public List<OutlineLine> OutlineLines {
			get {
				return _outlineLines;
			}
			private set {
				_outlineLines = value;
			}
		}

		[SerializeField]
		private List<Vector3> _points = new List<Vector3> ();
		/// <summary>
		/// Use SetPoint() to edit points
		/// </summary>
		public List<Vector3> Points {
			get {
				return _points;
			}
			private set {
				_points = value;
			}
		}

		/// <summary>
		/// Initialises with default points
		/// </summary>
		public OutlinePoly () : this (DefaultPoints) { }

		/// <summary>
		/// Generates polygon from list of points with no handlePoints on its lines
		/// </summary>
		public OutlinePoly (List<Vector3> points) {
			if (points.Count < 3) points = DefaultPoints; //Triangle is smaller acceptable polygon
			Points = points;
			InitializeLines ();
		}

		/// <summary>
		/// Generates polygon from list of points with no handlePoints on its lines
		/// </summary>
		public OutlinePoly (List<Vector3> points, int minHandles, int maxHandles, bool quasirandomHandles, int handleRandomness, bool sameHandlesForAllLines) {
			if (points.Count < 3) points = DefaultPoints; //Triangle is smaller acceptable polygon
			Points = points;
			if (sameHandlesForAllLines) {
				InitializeLinesSame (minHandles, maxHandles, quasirandomHandles, handleRandomness);
			} else {
				InitializeLinesDifferent (minHandles, maxHandles, quasirandomHandles, handleRandomness);
			}
		}

		/// <summary>
		/// Generates an OutlineLine with the specified index making up the polygon.
		/// There are the same number of lines as points. Indices of line based on start index of point
		/// </summary>
		/// <returns></returns>
		public OutlineLine GetOutlineLine (int index) {
			if (index < 0 || index >= Points.Count) throw new System.ArgumentOutOfRangeException ("Index out of range");

			int startPoint = index;
			int endPoint;
			if (index == Points.Count - 1) {
				endPoint = 0; //Last index, so loop back around
			} else {
				endPoint = index;
			}
			return new OutlineLine (this, startPoint, endPoint);

		}

		/// <summary>
		/// Sets a point with specified index. Also recalculates lines connected with that point.
		/// </summary>
		public void SetPoint (int pointIndex, Vector3 newPosition) {
			Points[pointIndex] = newPosition;
			RecalculateLinesFromPoint (pointIndex);
		}

		/// <summary>
		/// Generates lines with different number of HandlePoints and different relative positions
		/// </summary>
		private void InitializeLinesDifferent (int minHandles, int maxHandles, bool quasirandomHandles, int handleRandomness) {
			List<OutlineLine> lines = new List<OutlineLine> ();

			for (int i = 0; i < Points.Count - 1; i++) {
				lines.Add (new OutlineLine (this, i, i + 1, minHandles, maxHandles, quasirandomHandles, handleRandomness));
			}
			//Handling for last line where endpoint has to wrap around to index 0
			lines.Add (new OutlineLine (this, Points.Count - 1, 0, minHandles, maxHandles, quasirandomHandles, handleRandomness));

			OutlineLines = lines;
		}

		/// <summary>
		/// Generates lines with same number and relative positions of HandlePoints. Creates symmetry (probably. It's not like I tested it or anything)
		/// </summary>
		private void InitializeLinesSame (int minHandles, int maxHandles, bool quasirandomHandles, int handleRandomness) {
			List<float> handles;
			if (quasirandomHandles == true) {
				handles = OutlineLine.GenerateQuasirandomHandlePoints (minHandles, maxHandles, handleRandomness);
			} else {
				handles = OutlineLine.GenerateRandomHandlePoints (minHandles, maxHandles);
			}

			List<OutlineLine> lines = new List<OutlineLine> ();

			for (int i = 0; i < Points.Count - 1; i++) {
				lines.Add (new OutlineLine (this, i, i + 1, handles));
			}
			//Handling for last line where endpoint has to wrap around to index 0
			lines.Add (new OutlineLine (this, Points.Count - 1, 0, handles));

			OutlineLines = lines;
		}

		/// <summary>
		/// Generates lines with no handles
		/// </summary>
		private void InitializeLines () {
			foreach (Vector3 point in  Points) {
				OutlineLines.Add (new OutlineLine (this));
			}

			RecalculateAllLines ();
		}

		/// <summary>
		/// Adds line to end of list with 0 start and end points. Always use in conjuntion with RecalculateLinesAll
		/// </summary>
		private void AddLine () {
			OutlineLines.Add (new OutlineLine (this));
		}

		/// <summary>
		/// Recalculates specified line, i.e. if point is moved. Kind of untested. MIGHT NOT WORK. TEST.
		/// </summary>
		private void RecalculateLine (int lineIndex) {
			if (lineIndex < 0 || lineIndex >= Points.Count) throw new System.ArgumentOutOfRangeException ("Index out of range");
			if (Points.Count != OutlineLines.Count) {
				throw new System.Exception ("Number of points not equal to number of lines. Something went REALLY wrong.");
			}

			//Line to next point
			OutlineLines[lineIndex].StartPoint = lineIndex;
			if (lineIndex == Points.Count - 1) { //Wrap endpoint
				OutlineLines[lineIndex].EndPoint = 0; //Last point; wrap around to beginning
			}
			else {
				OutlineLines[lineIndex].EndPoint = lineIndex + 1;
			}
		}

		/// <summary>
		/// Recalculates lines connected to pointIndex
		/// </summary>
		private void RecalculateLinesFromPoint (int pointIndex) {
			RecalculateLine (pointIndex);
			RecalculateLine (pointIndex - 1);
		}

		private void RecalculateAllLines () {
			for (int i = 0; i < Points.Count; i++) {
				RecalculateLine (i);
			}
		}

		/// <summary>
		/// Adds point at pointIndex, shifting all subsequent points upwards
		/// </summary>
		/// <param name="pointIndex">Index to insert to</param>
		/// <param name="point">Point to insert</param>
		public void InsertPoint (int pointIndex, Vector3 point) {
			Points.Add (Vector3.zero);
			AddLine ();
			
			for (int i = Points.Count - 2; i >= pointIndex; i--) { //Shift all subsequent points upwards. -2 b/c we just added a point
				SwapPointIndex (i, i + 1);
			}
			Points[pointIndex] = point;

			RecalculateAllLines ();
		}

		public void RemovePoint (int pointIndex) {
			Points.RemoveAt (pointIndex);
			OutlineLines.RemoveAt (pointIndex);

			RecalculateAllLines ();
		}

		/// <summary>
		/// Swaps the points with index i1 and index i2 int the polygon's order
		/// </summary>
		public void SwapPointIndex (int i1, int i2) {
			Vector3 point1 = Points[i1];
			Vector3 point2 = Points[i2];

			SetPoint (i1, point2);
			SetPoint (i2, point1);
		}

		public void ShiftPointIndexUp (int pointIndex) {
			int nextIndex = (pointIndex == Points.Count - 1) ? 0 : pointIndex + 1;

			SwapPointIndex (pointIndex, nextIndex);
		}

		public void ShiftPointIndexDown (int pointIndex) {
			int prevIndex = (pointIndex == 0) ? Points.Count - 1 : pointIndex - 1;

			SwapPointIndex (pointIndex, prevIndex);
		}

		
	}
}