﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

using UnityEngine;
using EzySlice;

using Assets.PMesh.Unity;
using nobnak.Geometry;
using MeshDecimator;
using MeshDecimator.Unity;

namespace Assets.PMesh.Data.Patterns.Blocks {
	/// <summary>
	/// Pattern block which removes splices of mesh along x and y axis in rectangular grid
	/// </summary>
	class RectangularSplitBlock {
		public static void Apply (ProceduralMeshComponent pMesh, int numberOfCutsX, int numberOfCutsZ) {
			//Calculations of quantities
			float lengthX = pMesh.MeshF.mesh.bounds.size.x; //These should be in local coords
			float lengthZ = pMesh.MeshF.mesh.bounds.size.z;

			float distBetweenCutsX = lengthX / (float) (numberOfCutsX + 1); //Measured from centre of missing areas
			float distBetweenCutsZ = lengthZ / (float) (numberOfCutsZ + 1);

			float widthOfCutX = distBetweenCutsX * 0.5f; //Also how wide bit which is not missing is
			float widthOfCutZ = distBetweenCutsZ * 0.5f;

			//Split object
			//List<Mesh> meshesX = new List<Mesh> (); //Meshes that have been split in X direction only
			//Mesh workingMesh = mesh;

			/*List<GameObject> subsets = new List<GameObject> (); //List of cut parts
			subsets.Add (pMesh.gameObject); //Start off with full object

			for (int x = 0; x < numberOfCutsX; x++) { //Cut pieces on +ve side (think recursively) 
				CutMeshSectionX (subsets, pMesh, distBetweenCutsX, x);
				//yield return new WaitForSeconds (1);
			}



			pMesh.gameObject.SetActive (false);

			for (int i = 0; i < subsets.Count; i++) {
				subsets[i].GetComponent<MeshFilter> ().mesh.RecalculateNormals ();
				subsets[i].name = "Subset " + i.ToString ();
				GameObject.Instantiate (subsets[i]);
			}*/

			CoroutineHook.Instance.StartCoroutine (CutMeshSectionXRecursive (pMesh.gameObject, pMesh, distBetweenCutsX, 0));
		}

		private static void CutMeshSectionX (List<GameObject> subsets, ProceduralMeshComponent pMesh, float distBetweenCutsX, int indX) {
			Vector3 origin = new Vector3 (-pMesh.MeshF.mesh.bounds.extents.x, -pMesh.MeshF.mesh.bounds.extents.z);

			Vector3 planePosN = new Vector3 (origin.x + distBetweenCutsX + indX * distBetweenCutsX - distBetweenCutsX * 0.25f, 0, 0); //negative
			Vector3 planePosP = new Vector3 (origin.x + distBetweenCutsX + indX * distBetweenCutsX + distBetweenCutsX * 0.25f, 0, 0); //positive

			planePosN = new Vector3 ((float) Math.Round (planePosN.x, 2), (float) Math.Round (planePosN.y, 2), (float) Math.Round (planePosN.z, 2));
			planePosP = new Vector3 ((float) Math.Round (planePosP.x, 2), (float) Math.Round (planePosP.y, 2), (float) Math.Round (planePosP.z, 2));

			NDPlane splittingPlaneN = new NDPlane (planePosN, Vector3.right); //Define cutting planes
			NDPlane splittingPlaneP = new NDPlane (planePosP, Vector3.right);

			//GameObject vertPlane = Resources.Load ("verticalPlane") as GameObject;
			//GameObject.Instantiate (vertPlane, planePosN, Quaternion.identity);
			//GameObject.Instantiate (vertPlane, planePosP, Quaternion.identity);

			//Assuming mesh[1] is +ve mesh and mesh[0] is -ve
			GameObject objectToCut = subsets[subsets.Count - 1]; //Always working with last element (most +ve)
			List<GameObject> gos0 = MeshSlicer.CutObjectInstantiate (objectToCut, splittingPlaneN); //-ve plane cut
			List<GameObject> gos1 = MeshSlicer.CutObjectInstantiate (gos0[1], splittingPlaneP); //+ve plane cut

			GameObject negativeHalf = gos0[0];
			GameObject positiveHalf = gos1[1];

			GameObject.Destroy (gos0[0]); //Dont need this one
			GameObject.Destroy (gos0[1]); //Dont need this one
			GameObject.Destroy (gos1[0]); //Dont need this one
			GameObject.Destroy (gos1[1]); //Dont need this one

			subsets.RemoveAt (subsets.Count - 1); //Last element removed - 'transformed' into 2 subsets
			subsets.Add (negativeHalf);
			subsets.Add (positiveHalf);

			//var objectsToCombine = new GameObject[2] { gos0[0], gos1[1] };

			//var combiner = pMesh.gameObject.AddComponent<MeshCombiner> ();
			//combiner.Combine (objectsToCombine);

			//pMesh.MeshF.mesh.name = "Combined Mesh";
		}

		private static IEnumerator CutMeshSectionXRecursive (GameObject objectToCut, ProceduralMeshComponent pMesh, float distBetweenCutsX, int indX) {
			Vector3 origin = new Vector3 (-pMesh.MeshF.mesh.bounds.extents.x, -pMesh.MeshF.mesh.bounds.extents.z);

			MeshSlicer.Reset ();
			Vector3 planePosN = new Vector3 (origin.x + distBetweenCutsX + indX * distBetweenCutsX - distBetweenCutsX * 0.25f, 0, 0); //negative
			MeshSlicer.Reset ();
			Vector3 planePosP = new Vector3 (origin.x + distBetweenCutsX + indX * distBetweenCutsX + distBetweenCutsX * 0.25f, 0, 0); //positive

			NDPlane splittingPlaneN = new NDPlane (planePosN, Vector3.right); //Define cutting planes
			NDPlane splittingPlaneP = new NDPlane (planePosP, Vector3.right);

			//GameObject vertPlane = Resources.Load ("verticalPlane") as GameObject;
			//GameObject.Instantiate (vertPlane, planePosN, Quaternion.identity);
			//GameObject.Instantiate (vertPlane, planePosP, Quaternion.identity);

			//Assuming mesh[1] is +ve mesh and mesh[0] is -ve
			List<GameObject> gos0 = MeshSlicer.CutObjectInstantiate (objectToCut, splittingPlaneN); //-ve plane cut
			List<GameObject> gos1 = MeshSlicer.CutObjectInstantiate (gos0[1], splittingPlaneP); //+ve plane cut

			GameObject.Destroy (gos0[1]); //Dont need this one
			GameObject.Destroy (gos1[0]); //Dont need this one

			var decimator0 = gos0[0].AddComponent<DecimatorTest> ();
			var decimator1 = gos1[1].AddComponent<DecimatorTest> ();

			//decimator0.Decimate ();
			//decimator1.Decimate ();

			//gos0[0]
			var meshF00 = gos0[0].GetComponent<MeshFilter> ();
			var simp0 = new Simplification (meshF00.mesh.vertices, meshF00.mesh.triangles);

			simp0.boundaryPenalty = int.MaxValue;
			simp0.normalFlippingPenalty = int.MaxValue;

			Vector3[] outVerts0;
			int[] outTris0;

			simp0.ToMesh (out outVerts0, out outTris0);
			meshF00.mesh.vertices = outVerts0;
			meshF00.mesh.triangles = outTris0;

			//gos1[1]
			var meshF11 = gos1[1].GetComponent<MeshFilter> ();
			var simp1 = new Simplification (meshF11.mesh.vertices, meshF11.mesh.triangles);

			simp1.boundaryPenalty = int.MaxValue;
			simp1.normalFlippingPenalty = int.MaxValue;

			Vector3[] outVerts1;
			int[] outTris1;

			simp1.ToMesh (out outVerts1, out outTris1);
			meshF11.mesh.vertices = outVerts1;
			meshF11.mesh.triangles = outTris1;


			//Simplify
			//var meshF00 = gos0[0].GetComponent<MeshFilter> ();
			var meshOLD0 = meshF00.mesh;
			UnityEngine.Mesh outMesh = MeshDecimatorUtility.DecimateMesh (meshF00.mesh, Matrix4x4.identity, 0.1f, false);
			gos0[0].GetComponent<MeshFilter> ().mesh = outMesh;

			//var meshF11 = gos1[1].GetComponent<MeshFilter> ();
			gos1[1].GetComponent<MeshFilter> ().mesh = MeshDecimatorUtility.DecimateMesh (meshF11.mesh, Matrix4x4.identity, 0.1f, false);

			gos0[0].AddComponent<DrawVertices> ();
			gos1[1].AddComponent<DrawVertices> ();
			gos0[0].AddComponent<mattatz.MeshBuilderSystem.TriangleHelper> ();
			gos1[1].AddComponent<mattatz.MeshBuilderSystem.TriangleHelper> ();

			bool b = objectToCut.name == pMesh.name;
			if (b == false) GameObject.Destroy (objectToCut); //Delete whatever we just split, but we still need pMesh so dont delete that

			pMesh.gameObject.SetActive (false);

			yield return new WaitForSeconds (1);

			if (indX < 6 - 1) {
				indX += 1;
				CoroutineHook.Instance.StartCoroutine (CutMeshSectionXRecursive (gos1[1], pMesh, distBetweenCutsX, indX));
			}
		}

		#region helpers
		private static GameObject GenerateMeshObject (UnityEngine.Mesh mesh, string name) {
			Material coolMat = (Material) Resources.Load ("Material/UV");

			GameObject GO = new GameObject (name);
			GO.AddComponent<MeshFilter> ().mesh = mesh;
			GO.AddComponent<MeshRenderer> ().materials = new Material[] { coolMat, coolMat };
			return GO;
		}
		#endregion
	}
}
