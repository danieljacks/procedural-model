﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.PMesh.Data.Outlines {
	/// <summary>
	/// Base class for outlines
	/// </summary>
	public class Outline {
		/// <summary>
		/// Offset of outline component. This is probably relative to the parent transform / outlineCollection
		/// </summary>
		public Vector3 Offset { get; set; }

		public Outline () { } //Default constructor used for things like instantiating classes to store their types

		public Outline (int someint) { }
	}
}
