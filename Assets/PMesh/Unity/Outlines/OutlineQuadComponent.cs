﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.PMesh.Data.Outlines;

namespace Assets.PMesh.Unity {
	public class OutlineQuadComponent : OutlineComponent {
		private OutlineQuad _quad = OutlineQuad.DefaultQuad;

		public OutlineQuad OutlineQuad {
			get { return _quad; }
			set { _quad = value; }
		}

		public override Outline Outline {
			get { return _quad; }
			set { _quad = (OutlineQuad) value; }
		}

		public override void OnValidate () {
			base.OnValidate ();
		}
	}

}
