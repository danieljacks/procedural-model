﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Assets.PMesh.Data.Outlines;
using Assets.PMesh.Data.Patterns;

namespace Assets.PMesh.Unity {
	//[RequireComponent(typeof(OutlinePolyComponent))]
	[RequireComponent(typeof(MeshFilter))]
	public class ProceduralMeshComponent : MonoBehaviour {
		private MeshFilter _meshF;
		//private List<OutlinePolyComponent> _outlinePolyComponents;
		//private Pattern _pattern;

		/// <summary>
		/// Lazy instantiated
		/// </summary>
		public MeshFilter MeshF {
			get {
				_meshF = GetComponent<MeshFilter>();
				return _meshF;
			}
			set { _meshF = value;
			}
		}
		//Lazy instantiated
		/*public List<OutlinePolyComponent> OutlinePolyComponents {
			get {
				_outlinePolyComponents = _outlinePolyComponents ?? new List<OutlinePolyComponent> (GetComponents<OutlinePolyComponent> ());
				return _outlinePolyComponents;
			}
			set {
				_outlinePolyComponents = value;
			}
		}*/
	}
}
