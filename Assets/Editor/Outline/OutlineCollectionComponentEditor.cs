﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

using Assets.PMesh.Unity;
using Assets.PMesh.Data.Outlines;
using Assets.Helpers;

namespace Assets.Editor {
	[CustomEditor(typeof(OutlineCollectionComponent))]
	class OutlineCollectionComponentEditor : UnityEditor.Editor {

		#region fields
		OutlineCollection collection;
		OutlineCollectionComponent collectionComponent;

		DropdownWithButton<Outline> dropdownWithButton;

		static Dictionary<Type, OutlineEditor> outlineEditorDictionary = new Dictionary<Type, OutlineEditor> ();
		#endregion

		public void OnEnable () {
			collectionComponent = (OutlineCollectionComponent) target;
			collection = collectionComponent.OutlineCollection;

			dropdownWithButton = new DropdownWithButton<Outline> (GetOutlineTypes, AddOutline, "Add ");

			RefreshOutlineEditorsDictionary ();
		}

		#region Inspector
		public override void OnInspectorGUI () {
			EditorGUILayout.LabelField ("Outline components in collection: ");

			EditorGUILayout.BeginVertical ();
			if (collection.Outlines.Count > 0) {

				for (int i = collection.Outlines.Count - 1; i >= 0; i--) {
					DrawOutlineEntry (i);
				}

			} else {
				EditorGUILayout.HelpBox ("No outline components in collection", MessageType.None);
			}

			EditorGUILayout.EndVertical ();

			dropdownWithButton.DrawDropdownWithButton ();
		}

		//Full entry with delete button
		private void DrawOutlineEntry (int i) {
			EditorGUILayout.BeginHorizontal ();

			if (collection.Outlines.Count > 0) {
				DrawOutlineInspector (collection.Outlines[i]);
			}

			if (GUILayout.Button ("X", EditorStyles.miniButton, GUILayout.ExpandWidth (false))) { //Delete button + confirmation dialogue
				if (EditorUtility.DisplayDialog ("Delete", "Are you sure you want to delete this " + collection.Outlines[i].GetType ().Name + "? /n This cannot be undone", "Delete", "Don't Delete")) {
					collection.Outlines.RemoveAt (i);
				}
			}

			EditorGUILayout.EndHorizontal ();
		}

		//Only part which is defined by custom outline editor class
		private void DrawOutlineInspector (Outline outline) {

			OutlineEditor outlineEditor;
			Type tempType = outline.GetType ();
			if (outlineEditorDictionary.TryGetValue (outline.GetType(), out outlineEditor) == false) { //Found an editor to use in cached editors
				outlineEditor = new OutlineEditor (); //Use default if none found
			}

			outlineEditor.DrawInspector (outline, collectionComponent.transform); //Draw the inspector
		}

		#endregion

		#region Reflection
		private OutlineEditor GetEditorInstance (Outline outline) {
			List<OutlineEditor> editors = ReflectionHelper.GetChildrenWithAttribute<OutlineEditor, OutlineEditorAttribute> ();

			OutlineEditor correctEditor = (from editor in editors
										   from attribute in (Attribute[]) editor.GetType ().GetCustomAttributes (false)
										   where attribute.GetType () == typeof (OutlineEditorAttribute)
										   let editorAttribute = (OutlineEditorAttribute) attribute
										   where editorAttribute.OutlineType == outline.GetType ()
										   select editor).FirstOrDefault ();

			if (correctEditor == default (OutlineEditor)) return new OutlineEditor (); //No editor found

			return correctEditor; //Found a valid editor to use
		}

		private List<Outline> GetOutlineTypes () {
			var temp = ReflectionHelper.GetChildren<Outline> ();
			return temp;
		}

		[DidReloadScripts]
		private static void RefreshOutlineEditorsDictionary () {
			List<OutlineEditor> editors = ReflectionHelper.GetChildrenWithAttribute<OutlineEditor, OutlineEditorAttribute> ();

			IEnumerable<KeyValuePair<object, object>> entries //find all outline types with associated editor
							= from editor in editors
							  from attribute in (Attribute[]) editor.GetType ().GetCustomAttributes (false)
							  where attribute.GetType () == typeof (OutlineEditorAttribute)
							  let editorAttribute = (OutlineEditorAttribute) attribute
							  where editorAttribute.IsValid ()
							  select new KeyValuePair<object, object> (editorAttribute.OutlineType, editor);

			outlineEditorDictionary.Clear (); //Update dictionary with new values
			var tempEnties = entries.ToList ();
			entries.ForEach (e => outlineEditorDictionary.Add ((Type) e.Key, (OutlineEditor) e.Value));
		}
		#endregion

		private void AddOutline (Outline outlineType) {
			collection.Outlines.Add ((Outline) Activator.CreateInstance (outlineType.GetType ()));
		} 

		#region Scene
		private void DrawOutlineScenes () {
			foreach (Outline outline in collection.Outlines) {
				DrawOutlineInspector (outline);
			}
		}
		#endregion
	}
}
