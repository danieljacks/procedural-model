﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.PMesh.Data.Patterns {
	/// <summary>
	/// Base class for pattern input parameters.
	/// PatternParameters hold pattern method inputs but also contain functionality for randomly generating parameters
	/// </summary>
	public abstract class PatternParameters {
	}
}
