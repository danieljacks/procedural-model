﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.PMesh.Unity;
using Assets.PMesh.Data.Outlines;

namespace Assets.Editor {
	[CustomEditor(typeof(OutlineQuadComponent))]
	public class OutlineQuadComponentEditor : UnityEditor.Editor {
		OutlineQuadComponent quadComponent;
		OutlineQuad quad;

		OutlineQuadInspector quadInspector = new OutlineQuadInspector ();
		PolyDrawer_OLD drawer = new PolyDrawer_OLD (Color.black, Color.red, 0.2f);

		private void OnEnable () {
			quadComponent = (OutlineQuadComponent) target;
			quad = quadComponent.OutlineQuad;

			SceneView.onSceneGUIDelegate -= DrawScene;
			SceneView.onSceneGUIDelegate += DrawScene;
		}

		private void OnDisable () {
			SceneView.onSceneGUIDelegate -= DrawScene;
		}

		public override void OnInspectorGUI () {
			quadInspector.DrawInspector (quadComponent);
		}

		public void DrawScene (SceneView view) {
			drawer.DrawPoly (quad, quadComponent.transform);
		}
	}
}
