﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.PMesh.Data.Outlines {
	public class OutlineCollection {
		public List<Outline> Outlines { get; set; }

		public OutlineCollection (List<Outline> outlines) {
			Outlines = outlines;
		}

		public OutlineCollection () : this (new List<Outline> ()) { }

		/// <summary>
		/// Returns all elements of Outlines list that are either the specified type, or derived from the specified type.
		/// Although these are only copies, they are classes and so passed by reference. Directly modifying values should still modify originals
		/// </summary>
		public List<T> FilterOutlines<T> () where T : Outline {
			return Outlines
					.Where (o => o.GetType ().IsSubclassOf (typeof (T)) || o.GetType () == typeof (T))
					.Select (o => (T) o)
					.ToList ();
		}
	}
}
