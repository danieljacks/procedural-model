﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.PMesh.Data.Outlines;

using UnityEngine;

namespace Assets.PMesh.Unity {
	[Serializable]
	public class OutlinePolyComponent : OutlineComponent {
		[SerializeField]
		private OutlinePoly _poly = OutlinePoly.DefaultPoly;

		public OutlinePoly OutlinePoly {
			get { return _poly; }
			set { _poly = value; }
		}

		public override Outline Outline {
			get { return _poly; }
			set { _poly = (OutlinePoly) value; }
		}

		public override void OnValidate () {
			base.OnValidate ();
		}
	}
}