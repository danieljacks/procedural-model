﻿using Assets.PMesh.Data.Outlines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.PMesh.Data.Outlines {
	/// <summary>
	/// Bot left considered origin, bot right is (width, 0), top left is (0, height)
	/// </summary>
	public class OutlineQuad : OutlinePoly {

		#region Static
		/// <summary>
		/// Default set of points that a poly is initialised with
		/// </summary>
		public static OutlineQuad DefaultQuad {
			get {
				return new OutlineQuad (
					new OutlineNode (new Vector3 (0, 0, 0)),
					new OutlineNode (new Vector3 (1, 0, 0)),
					new OutlineNode (new Vector3 (1, 1, 0)),
					new OutlineNode (new Vector3 (0, 1, 0)));
			}
		}
		#endregion

		#region Fields
		private float _width;
		private float _height;

		//For use with UVToXY and XYToUV methods
		private Vector2 normal0;
		private Vector2 normal1;
		private Vector2 normal2;
		private Vector2 normal3;
		//END
		#endregion

		#region Properties
		public override List<OutlineNode> Nodes {
			get {
				return base.Nodes;
			}
			set {
				base.Nodes = value;
				if (Nodes.Count >= 5) {
					throw new System.ArgumentException ("Rectangle can't have " + Nodes.Count + " vertices!");
				}
				UpdateDimensions ();
			}
		}

		/// <summary>
		/// Distance from BotLeft to BotRight point
		/// </summary>
		public float Width {
			get {
				return _width;
			}
		}

		/// <summary>
		/// Distance from BotLeft to TopLeft point
		/// </summary>
		public float Height {
			get {
				return _height;
			}
		}

		public OutlineNode BotLeft {
			get {
				return Nodes[0];
			}
			set {
				Nodes[0] = value;
			}
		}
		public OutlineNode BotRight {
			get {
				return Nodes[1];
			}
			set {
				Nodes[1] = value;
			}
		}
		public OutlineNode TopRight {
			get {
				return Nodes[2];
			}
			set {
				Nodes[2] = value;
			}
		}
		public OutlineNode TopLeft {
			get {
				return Nodes[3];
			}
			set {
				Nodes[3] = value;
			}
		}
		#endregion

		#region Constructors
		public OutlineQuad (OutlineNode botLeft, OutlineNode botRight, OutlineNode topRight, OutlineNode topLeft)
			: base (new List<OutlineNode> () { botLeft, botRight, topRight, topLeft }) { }

		public OutlineQuad (Vector3 botLeft, Vector3 botRight, Vector3 topRight, Vector3 topLeft)
			: base (new List<Vector3> () { botLeft, botRight, topRight, topLeft }) { }

		public OutlineQuad () : this (DefaultQuad.BotLeft, DefaultQuad.BotRight, DefaultQuad.TopRight, DefaultQuad.TopLeft) { }
		#endregion

		#region PrivateMethods
		private void UpdateDimensions () {
			for (int i = 0; i < Nodes.Count; i++) { //ensure there is no depth in nodes
				if (Nodes[i].Position.z != 0) {
					_nodes[i].Position = new Vector3 (_nodes[i].Position.x, _nodes[i].Position.y, 0);
					Debug.LogError ("OutlineQuad nodes cannot have depth! Setting z component of Node[" + i + "] to 0");
				}
			}

			_width = Vector3.Distance (BotLeft.Position, BotRight.Position);
			_height = Vector3.Distance (BotLeft.Position, TopLeft.Position);
		}

		//from http://answers.unity3d.com/questions/1302113/check-if-a-point-is-on-the-right-or-left-of-a-vect.html
		bool IsLeft (Vector2 A, Vector2 B) {
			return (-A.x * B.y + A.y * B.x < 0);
		}
		#endregion

		#region PublicMethods
		/// <summary>
		/// Transforms UV coordinates to real space XY coordinates
		/// sourced from https://math.stackexchange.com/questions/13404/mapping-irregular-quadrilateral-to-a-rectangle (was originally, but changed. Actual algorithm is mine)
		/// </summary>
		/// <param name="UVCoords">UV coords are in the 0..1 range and each represent opposite pairs of sides of the quad. Use like x and y in a rectangle and everything works out</param>
		/// <returns>XY coordinate vector (in relative space)</returns>
		public Vector2 UVToXY (Vector2 UVCoords) {
			float u = UVCoords.x;
			float v = UVCoords.y;

			Vector3 a = BotLeft.Position;
			Vector3 b = BotRight.Position;
			Vector3 c = TopRight.Position;
			Vector3 d = TopLeft.Position;

			Vector3 ab = Vector3.LerpUnclamped (a, b, u);
			Vector3 dc = Vector3.LerpUnclamped (d, c, u);

			Vector3 p = Vector3.LerpUnclamped (ab, dc, v);

			return p;
		}
		#endregion
	}
}