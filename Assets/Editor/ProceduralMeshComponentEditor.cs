﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Assets.PMesh.Unity;

namespace Assets.Editor {
	[CustomEditor(typeof(ProceduralMeshComponent))]
	class ProceduralMeshComponentEditor : UnityEditor.Editor {
		ProceduralMeshComponent pMesh;

		private void OnEnable () {
			pMesh = (ProceduralMeshComponent) target;
		}

		public override void OnInspectorGUI () {
			DrawDefaultInspector ();
			if (Application.isPlaying) {
				if (GUILayout.Button ("Slice!")) {
					//pMesh.ApplyPattern ();
				}
			} else {
				EditorGUILayout.HelpBox ("Must be in play mode to slice!", MessageType.Info);
			}
			
		}
	}
}
