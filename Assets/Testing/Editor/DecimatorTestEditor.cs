﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DecimatorTest))]
public class DecimatorTestEditor : UnityEditor.Editor {
	DecimatorTest decimatorTest;

	private void OnEnable () {
		decimatorTest = target as DecimatorTest;
	}

	public override void OnInspectorGUI () {
		DrawDefaultInspector ();
		if (GUILayout.Button ("Decimate")) {
			decimatorTest.Decimate ();
		}
	}

}
