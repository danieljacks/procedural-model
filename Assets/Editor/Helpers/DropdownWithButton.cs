﻿using System;
using System.Collections.Specialized;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

/// <summary>
/// Creates a field in inspector containing a dropdown list and button.
/// The button activates a user-defined callback method (could be anything), with the selected item of type T as the input parameter
/// </summary>
public class DropdownWithButton<T> where T : class {

	int _selectedIndex = -1;
	int selectedIndex {
		get {
			return Math.Min (cachedTypes.Count - 1, _selectedIndex); //Keep within range, just clamp if it isnt
		}
		set {
			_selectedIndex = value;
		}
	}
	string selectedDropdownText = "Select Type";
	string selectedDropdownTextDefault = "Select Type";

	private List<T> cachedTypes = new List<T> ();

	public Func<List<T>> RefreshTypesMethod { get; set; }
	public Action<T> CallbackMethod { get; set; }
	/// <summary>
	/// Text to display on button, where t.GetType().Name is appended to the end
	/// </summary>
	public string ButtonString { get; set; }

	/// <summary>
	/// Creates new dropdown with buttons drawer instance
	/// </summary>
	/// <param name="refreshDictionaryMethod">Method that retrieves new instance of the dictionary used, with types in dropdown list and their names</param>
	/// <param name="callbackMethod">Method to call on successful clicking of button</param>
	/// <param name="buttonString">Text to display on button, where t.GetType().Name is appended to the end</param>
	public DropdownWithButton (Func<List<T>> refreshTypesMethod, Action<T> callbackMethod, string buttonString) {
		RefreshTypesMethod = refreshTypesMethod;
		CallbackMethod = callbackMethod;
		ButtonString = buttonString;

		selectedIndex = -1;
	}

	public void DrawDropdownWithButton () {
		EditorGUILayout.BeginHorizontal ();

		DrawButton ();

		DrawDropdown ();

		EditorGUILayout.EndHorizontal ();
	}

	private void DrawButton () {
		if (selectedIndex == -1) {
			EditorGUILayout.HelpBox ("Please choose a type", MessageType.None);
			//selectedDropdownText = selectedDropdownTextDefault;
			return;
		}

		//List<T> updatedTypes = RefreshTypesMethod ();
		//if (ListEquivalence (cachedTypes, updatedTypes) == false) {
		//	EditorGUILayout.HelpBox ("It seems the items have changed. Please reselect an item", MessageType.None);
		//	return;
		//}

		if (GUILayout.Button (ButtonString + cachedTypes[selectedIndex].GetType ().Name)) {
			CallbackMethod (cachedTypes[selectedIndex]);
		}
	}

	private void DrawDropdown () {
		if (EditorGUILayout.DropdownButton (new GUIContent (selectedDropdownText), FocusType.Keyboard)) {
			if (selectedIndex == -1) selectedIndex = 0;

			cachedTypes = RefreshTypesMethod (); //Update dictionary on dropdown open

			selectedIndex = Math.Min (selectedIndex, cachedTypes.Count - 1); //Clamp within strings index range

			string[] dropdownStrings = cachedTypes.Select (t => t.GetType ().Name).ToArray ();

			//selectedIndex = EditorGUILayout.Popup (selectedIndex, dropdownStrings); //Show popup
			GenericMenu menu = new GenericMenu ();
			for (int i = 0; i < dropdownStrings.Length; i++) {
				menu.AddItem (new GUIContent (dropdownStrings[i]), false, 
					ind => {
						SelectItemCallback ((int) ind);
					},
					i); //Shows cutom dropdown and updates selectedIndex on select

			}
			menu.ShowAsContext ();
		}
	}

	private void SelectItemCallback (int ind) {
		selectedIndex = (int) ind;
		selectedDropdownText = cachedTypes[ind].GetType().Name;
	}

	/// <summary>
	/// Only checks types of keys. Compares values normally
	/// </summary>
	private bool DictionaryEquivalence (Dictionary<T, string> d1, Dictionary<T, string> d2) {
		if (d1 == null || d2 == null) return false;
		if (d1.Count != d2.Count) return false;

		foreach (KeyValuePair<T, string> pair1 in d1) {
			bool match = false;
			foreach (KeyValuePair<T, string> pair2 in d2) {
				if (pair1.Key.GetType() == pair2.Key.GetType () && pair1.Value == pair2.Value) {
					match = true;
				}
			}
			if (match == false) return false; //An entry does not have an equivalent entry
		}
		return true; //All tests passed. They are equivalent for our purposes
	}

	/// <summary>
	/// Only checks GetType() of items
	/// </summary>
	private bool ListEquivalence (List<T> list1, List<T> list2) {
		if (list1 == null || list2 == null) return false;
		if (list1.Count != list2.Count) return false;

		foreach (T item1 in list1) {
			bool match = false;
			foreach (T item2 in list2) {
				if (item1.GetType () == item2.GetType ()) {
					match = true;
				}
			}
			if (match == false) return false; //An entry does not have an equivalent entry
		}
		return true; //All tests passed. They are equivalent for our purposes
	}
}
