﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

using PMesh_old;
using PMesh_old.Data;

namespace Assets.Pmesh_old.EditorDrawers {
[CustomEditor (typeof (ProceduralMesh))]
public class ProceduralMeshEditor : UnityEditor.Editor {

	SerializedProperty outlinePolys;
	ProceduralMesh proceduralMesh;

	Transform meshTransform;
	Quaternion handleRotation;

	private List<bool> showPolyInspector = new List<bool> ();

	private int selectedPolyIndex = -1;
	private int selectedPointIndex = -1;
	private float dotHandleSize = 0.06f;
	private float dotPickSize = 0.08f;

	void OnEnable () {
		outlinePolys = serializedObject.FindProperty ("_outlinePolys");

		proceduralMesh = (ProceduralMesh) target;

		SceneView.onSceneGUIDelegate -= DrawMeshGUI;
		SceneView.onSceneGUIDelegate += DrawMeshGUI;

		for (int p = 0; p < proceduralMesh.OutlinePolys.Count; p++) {
			showPolyInspector.Add (true);
		}
	}

	private void OnDisable () {
		SceneView.onSceneGUIDelegate -= DrawMeshGUI;
	}

	public override void OnInspectorGUI () {
		DrawInspector ();
	}

	#region helpers
	private void RecordUndo () {
		Undo.RecordObject (proceduralMesh, "Move Point");
		EditorUtility.SetDirty (proceduralMesh);
	}

	/// <summary>
	/// Adds poly to end of poly list
	/// </summary>
	private void AddPoly () {
		proceduralMesh.AddOutlinePoly (new OutlinePoly ());
		showPolyInspector.Add (true);
	}

	private void RemovePoly (int p) {
		if (proceduralMesh.OutlinePolys.Count <= 1) return;
		proceduralMesh.RemoveOutlinePoly (p);
		showPolyInspector.RemoveAt (p);
	}

	private void AddPoint (int polyInd, int pointInd) {
		proceduralMesh.OutlinePolys[polyInd].InsertPoint (pointInd, Vector3.zero);
	}

	private void RemovePoint (int polyIndex, int pointIndex) {
		if (proceduralMesh.OutlinePolys[polyIndex].Points.Count < 3) return; //Triangle is smallest acceptable polygon
		proceduralMesh.OutlinePolys[polyIndex].RemovePoint (pointIndex);
	}
	#endregion

	#region inspector

	private void DrawInspector () {
		float originalLabelWidth = EditorGUIUtility.labelWidth;
		float originalFieldWidth = EditorGUIUtility.fieldWidth;

		EditorGUIUtility.labelWidth = 20f;
		EditorGUIUtility.fieldWidth = 10f;

		if (GUILayout.Button ("Add Poly")) {
			AddPoly ();
		}

		for (int p = 0; p < proceduralMesh.OutlinePolys.Count; p++) { //Draw each polygon's inspector representation
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.BeginVertical ();
			if (showPolyInspector[p] = EditorGUILayout.Foldout (showPolyInspector[p], "Polygon " + p.ToString (), true)) {
				DrawInspectorPoly (p);
			}
			EditorGUILayout.EndVertical ();
			if (GUILayout.Button ("X", EditorStyles.miniButton)) { //This should be after all poly drawing

			}
			EditorGUILayout.EndHorizontal ();
		}

		EditorGUIUtility.labelWidth = originalLabelWidth;
		EditorGUIUtility.fieldWidth = originalFieldWidth;
	}

	private void DrawInspectorPoly (int p) {
		//Draw Vector3 fields for points
		DrawPolyInspectorVectors (p);

		//Draw labels for lines
		DrawPolyInspectorLines (p);

		if (GUILayout.Button ("Add point")) {
			AddPoint (p, selectedPointIndex < 0 ? 0 : selectedPointIndex );
		}
	}

	private void DrawPolyInspectorVectors (int p) {
		int pointsCount = proceduralMesh.OutlinePolys[p].Points.Count;

		if (pointsCount == 0) return;

		for (int i = 0; i < pointsCount; i++) {
			Vector3 newVector;
			Vector3 currentVal = proceduralMesh.OutlinePolys[p].Points[i];
			EditorGUI.BeginChangeCheck ();
			EditorGUILayout.PrefixLabel ("Point " + i.ToString ()); //Show point number
			newVector = EditorGUILayout.Vector3Field ("", currentVal);
			if (EditorGUI.EndChangeCheck ()) {
				RecordUndo ();
				proceduralMesh.OutlinePolys[p].SetPoint (i, newVector);
			}
		}
	}

	private void DrawPolyInspectorLines (int p) {
		//Lines can't be changed from inspector for now
		int linesCount = proceduralMesh.OutlinePolys[p].OutlineLines.Count;

		for (int i = 0; i < linesCount; i++) {
			EditorGUILayout.BeginHorizontal ();
			{
				EditorGUILayout.LabelField ("Line " + i.ToString () + ":"); //Line number
				EditorGUILayout.BeginVertical ();
				{
					EditorGUILayout.LabelField (proceduralMesh.OutlinePolys[p].OutlineLines[i].StartPoint.ToString ()); //Start point number
					EditorGUILayout.BeginHorizontal (); //Start point vector
					{
						EditorGUILayout.LabelField ("x: " + proceduralMesh.OutlinePolys[p].OutlineLines[i].StartPointVector.x.ToString () + ", ");
						EditorGUILayout.LabelField ("y: " + proceduralMesh.OutlinePolys[p].OutlineLines[i].StartPointVector.y.ToString () + ", ");
						EditorGUILayout.LabelField ("z: " + proceduralMesh.OutlinePolys[p].OutlineLines[i].StartPointVector.z.ToString ());
						EditorGUILayout.EndHorizontal ();
					}
					EditorGUILayout.EndVertical ();
				}
				EditorGUILayout.LabelField ("-->");
				EditorGUILayout.BeginVertical ();
				{
					EditorGUILayout.LabelField (proceduralMesh.OutlinePolys[p].OutlineLines[i].EndPoint.ToString ()); //End point number
					EditorGUILayout.BeginHorizontal (); //End point vector
					{
						EditorGUILayout.LabelField ("x: " + proceduralMesh.OutlinePolys[p].OutlineLines[i].EndPointVector.x.ToString ());
						EditorGUILayout.LabelField (", y: " + proceduralMesh.OutlinePolys[p].OutlineLines[i].EndPointVector.y.ToString ());
						EditorGUILayout.LabelField (", z: " + proceduralMesh.OutlinePolys[p].OutlineLines[i].EndPointVector.z.ToString ());
						EditorGUILayout.EndHorizontal ();
					}
					EditorGUILayout.EndVertical ();
				}
				EditorGUILayout.EndHorizontal ();
			}
		}
	}

#endregion

	#region scene
	public void DrawMeshGUI (SceneView sceneView) {
		DrawVertexHandles ();
		DrawPolys ();
	}

	public void DrawVertexHandles () {
		//Create points for all poly vertices in all polys
		for (int i = 0; i < proceduralMesh.OutlinePolys.Count; i++) {
			for (int p = 0; p < proceduralMesh.OutlinePolys[i].Points.Count; p++) {
				DrawHandle (i, p);
			}
		}
	}

	/// <summary>
	/// Draws handles and handles editing of point position
	/// </summary>
	private void DrawHandle (int polyIndex, int pointIndex) {
		meshTransform = proceduralMesh.transform;
		handleRotation = Tools.pivotRotation == PivotRotation.Local ? //Comply with local or global handle settings
			meshTransform.rotation : Quaternion.identity;

		Vector3 point = proceduralMesh.transform.TransformPoint (proceduralMesh.OutlinePolys[polyIndex].Points[pointIndex]);

		Handles.color = Color.white;
		float size = HandleUtility.GetHandleSize (point);
		//Debug.Log ("dotSize: " + dotSize.ToString ());
		if (Handles.Button (point, handleRotation, size * dotHandleSize, size * dotPickSize, Handles.DotHandleCap)) {
			selectedPolyIndex = polyIndex;
			selectedPointIndex = pointIndex;
		}

		if (polyIndex == selectedPolyIndex && pointIndex == selectedPointIndex) {
			EditorGUI.BeginChangeCheck ();
			point = proceduralMesh.transform.InverseTransformPoint (Handles.DoPositionHandle (point, handleRotation));

			if (EditorGUI.EndChangeCheck ()) {
				if (!EditorApplication.isPlaying) {//Can only do this in editmode
					Undo.RecordObject (proceduralMesh, "Move Point");
					EditorUtility.SetDirty (proceduralMesh);
					proceduralMesh.OutlinePolys[polyIndex].Points[pointIndex] = point;
				}
				//proceduralMesh.OutlinePolys[polyIndex].Points[pointIndex] = meshTransform.InverseTransformPoint (point);
			}
			
		}
	}

	public void DrawPolys () {
		//Fill in all polys in mesh
		foreach (OutlinePoly poly in proceduralMesh.OutlinePolys) {
			DrawPoly (poly, Color.green, Color.blue, 0.2f);
		}
	}

	/// <summary>
	/// Draw the specified poly at its position
	/// </summary>
	private void DrawPoly (OutlinePoly poly, Color outlineColor, Color fillColor, float alpha) {
		Color originalColor = Handles.color;

		Handles.color = outlineColor;
		foreach (OutlineLine line in poly.OutlineLines) {
			//UnityHelper.DrawLine (line.StartPointVector, line.EndPointVector, 5f);
			Vector3 start = proceduralMesh.transform.TransformPoint (line.StartPointVector);
			Vector3 end = proceduralMesh.transform.TransformPoint (line.EndPointVector);
			Handles.DrawLine (start, end);
		}

		Handles.color = new Color (fillColor.r, fillColor.g, fillColor.b, alpha);
		Handles.DrawAAConvexPolygon ( poly.Points.Select (v => proceduralMesh.transform.TransformPoint(v)).ToArray () ); //Also transforms to world space

		Handles.color = originalColor;
	}
#endregion
}
}