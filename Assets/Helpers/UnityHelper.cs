﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class UnityHelper {

	public static void DrawLine (Vector3 p1, Vector3 p2, float width) {
		int count = Mathf.CeilToInt (width); // how many lines are needed.
		if (count == 1)
			Gizmos.DrawLine (p1, p2);
		else {
			Camera c = Camera.current;
			if (c == null) {
				Debug.LogError ("Camera.current is null");
   
				return;
			}
			Vector3 v1 = (p2 - p1).normalized; // line direction
			Vector3 v2 = (c.transform.position - p1).normalized; // direction to camera
			Vector3 n = Vector3.Cross (v1, v2); // normal vector
			for (int i = 0; i < count; i++) {
				Vector3 o = n * width * ((float) i / (count - 1) - 0.5f);
				Handles.DrawLine (p1 + o, p2 + o);
			}
		}
	}

	public static void DrawPlane (Vector3 position, Vector3 normal) {

		Vector3 v3;

		if (normal.normalized != Vector3.forward)
			v3 = Vector3.Cross (normal, Vector3.forward).normalized * normal.magnitude;
		else
			v3 = Vector3.Cross (normal, Vector3.up).normalized * normal.magnitude; ;

		var corner0 = position + v3;
		var corner2 = position - v3;
		var q = Quaternion.AngleAxis (90.0f, normal);
		v3 = q * v3;
		var corner1 = position + v3;
		var corner3 = position - v3;

		Debug.DrawLine (corner0, corner2, Color.green);
		Debug.DrawLine (corner1, corner3, Color.green);
		Debug.DrawLine (corner0, corner1, Color.green);
		Debug.DrawLine (corner1, corner2, Color.green);
		Debug.DrawLine (corner2, corner3, Color.green);
		Debug.DrawLine (corner3, corner0, Color.green);
		Debug.DrawRay (position, normal, Color.red);
	}
}
