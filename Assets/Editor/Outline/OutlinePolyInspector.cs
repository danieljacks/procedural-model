﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Assets.PMesh.Unity;
using Assets.PMesh.Data.Outlines;
using UnityEditor;

namespace Assets.Editor {
	/// <summary>
	/// Handles drawing of OutlinePoly inspector
	/// </summary>
	public class OutlinePolyInspector {
		private bool showNodes = false;

		public void DrawInspector (OutlinePolyComponent OutlineComponent) {
			OutlinePoly poly = OutlineComponent.OutlinePoly;

			if (showNodes = EditorGUILayout.Foldout (showNodes, "Nodes")) {
				for (int i = 0; i < poly.Nodes.Count; i++) {
					DrawNodeField (OutlineComponent, poly, i);
				}
				if (GUILayout.Button ("Add Node")) {
					poly.Nodes.Add (new OutlineNode (Vector3.zero));
				}
			}
			
		}

		private void DrawNodeField (OutlinePolyComponent PolyComponent, OutlinePoly poly, int i) {
			EditorGUILayout.BeginHorizontal ();

			EditorGUI.BeginChangeCheck ();

			Vector3 modifiedPos = EditorGUILayout.Vector3Field ("Node " + i.ToString () + ":", poly.Nodes[i].Position);

			if (EditorGUI.EndChangeCheck ()) { //Undo operations
				EditorUtility.SetDirty (PolyComponent);

				Undo.RegisterCompleteObjectUndo (PolyComponent, "Moved Node");
				PolyComponent.OutlinePoly.Nodes[i].Position = modifiedPos;

				SceneView.RepaintAll ();
			}

			if (GUILayout.Button ("X", EditorStyles.miniButton, GUILayout.Width(20), GUILayout.Height(20)) && poly.Nodes.Count >= 4) {
				poly.Nodes.RemoveAt (i);
				SceneView.RepaintAll ();
			}

			EditorGUILayout.EndHorizontal ();
		}
	}
}