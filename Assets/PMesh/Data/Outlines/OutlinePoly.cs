﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Helpers;

namespace Assets.PMesh.Data.Outlines {
	[Serializable]
	/// <summary>
	/// A collection of OutlineNodes. Edges are implicit.
	/// </summary>
	public class OutlinePoly : Outline {
		/// <summary>
		/// Default set of points that a poly is initialised with
		/// </summary>
		public static OutlinePoly DefaultPoly {
			get {
				List<OutlineNode> defaultPts = new List<OutlineNode> () {
					new OutlineNode (new Vector3 (0, 0, 0)),
					new OutlineNode (new Vector3 (1, 0, 0)),
					new OutlineNode (new Vector3 (1, 1, 0)),
					new OutlineNode (new Vector3 (0, 1, 0)) };
				return new OutlinePoly (defaultPts);
			}
		}

		[SerializeField]
		protected List<OutlineNode> _nodes;
		public virtual List<OutlineNode> Nodes { get { return _nodes; } set { _nodes = value; }}

		#region Constructors
		public OutlinePoly (List<OutlineNode> nodes, Vector3 offset) {
			Nodes = nodes;
			Offset = offset;
		}

		public OutlinePoly (List<OutlineNode> nodes) : this (nodes, Vector3.zero) { }

		/// <summary>
		/// Uses default properties for nodes
		/// </summary>
		/// <param name="nodes"></param>
		public OutlinePoly (List<Vector3> points) {
			var nodes = new List<OutlineNode> ();

			foreach (Vector3 point in points) {
				nodes.Add (new OutlineNode (point));
			}

			Nodes = nodes;
		}

		public OutlinePoly () : this (DefaultPoly.Nodes) {

		}

		#endregion

		#region PublicMethods
		/// <summary>
		/// Gets a node with wrapping
		/// </summary>
		public OutlineNode GetNode (int index) {
			int i = MathHelper.WrapAround (index, 0, Nodes.Count);
			return Nodes[i];
		}

		/// <summary>
		/// Gets the edge connecting a node to the next node. Wraps value if outside range.
		/// </summary>
		public OutlineEdge GetEdgeNext (int index) {
			return new OutlineEdge (GetNode(index).Position, GetNode(index + 1).Position);
		}  

		/// <summary>
		/// Gets the edge connecting a node to the previous node. Wraps value if outside range.
		/// </summary>
		public OutlineEdge GetEdgePrev (int index) {
			return new OutlineEdge (GetNode(index).Position, GetNode(index - 1).Position);
		}

		/// <summary>
		/// Get a list of all edges in the poly, starting from node0 and edge pointing to next node
		/// </summary>
		public List<OutlineEdge> GetAllEdges () {
			List<OutlineEdge> edges = new List<OutlineEdge> ();
			for (int i = 0; i < Nodes.Count; i++) {
				edges.Add (GetEdgeNext(i));
			}
			return edges;
		}
	#endregion
	}
}
