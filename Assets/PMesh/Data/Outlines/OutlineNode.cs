﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.PMesh.Data.Outlines {
	[Serializable]
	public class OutlineNode {
		[SerializeField]
		private Vector3 _position;
		[SerializeField]
		private Color _color;

		public static Color DefaultColor { get { return Color.blue; } }

		/// <summary>
		/// Position of node relative to parent poly
		/// </summary>
		public Vector3 Position { get {return _position; } set {_position = value; } }

		/// <summary>
		/// Color of node
		/// </summary>
		public Color Color { get { return _color; } set { _color = value; } }

		public OutlineNode (Vector3 position, Color color) {
			Color = color;
			Position = position;
		}

		public OutlineNode (Vector3 position) : this (position, DefaultColor) { }
	}
}
