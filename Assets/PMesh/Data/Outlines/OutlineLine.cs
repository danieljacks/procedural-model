﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.PMesh.Data.Outlines {
	public class OutlineLine {
		public Vector3 Point1 { get; set; }
		public Vector3 Point2 { get; set; }

		public OutlineLine (Vector3 point1, Vector3 point2) {
			Point1 = point1;
			Point2 = point2;
		}
	}
}