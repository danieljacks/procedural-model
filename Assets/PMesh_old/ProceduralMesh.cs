﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PMesh_old.Data;

namespace PMesh_old {
	/// <summary>
	/// Inspector representation of a procedural mesh
	/// </summary>
	public class ProceduralMesh : MonoBehaviour {

		[SerializeField]
		private List<OutlinePoly> _outlinePolys = new List<OutlinePoly> ();
		public List<OutlinePoly> OutlinePolys {
			get {
				return _outlinePolys;
			}
			set {
				_outlinePolys = value;
			}
		}

		public ProceduralMesh () {
			if (OutlinePolys.Count == 0) {
				OutlinePolys.Add (new OutlinePoly ());
			}
		}

		public void AddOutlinePoly (OutlinePoly poly) {
			OutlinePolys.Add (poly);
		}

		public void RemoveOutlinePoly (int polyIndex) {
			OutlinePolys.RemoveAt (polyIndex);
		}
	}
}