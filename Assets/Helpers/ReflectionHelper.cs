﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Helpers {
	public static class ReflectionHelper {

		/// <summary>
		/// Returns all classes which are children of parent type and are decorated with the attribute type
		/// </summary>
		public static List<TParent> GetChildrenWithAttribute <TParent, TAttribute> () where TAttribute : System.Attribute {
			return (from a in AppDomain.CurrentDomain.GetAssemblies ()
				   from t in a.GetTypes ()
				   where t.IsDefined (typeof (TAttribute), false) && t.IsSubclassOf (typeof(TParent))
				   select (TParent) Activator.CreateInstance(t)).ToList ();
		}

		/// <summary>
		/// Returns first class which is child of parent type and decorated with the attribute type
		/// </summary>
		public static TParent GetChildWithAttribute<TParent, TAttribute> () where TAttribute : System.Attribute {
			return (from a in AppDomain.CurrentDomain.GetAssemblies ()
					from t in a.GetTypes ()
					where t.IsDefined (typeof (TAttribute), false) && t.IsSubclassOf (typeof (TParent))
					select (TParent) Activator.CreateInstance (t)).FirstOrDefault ();
		}

		/// <summary>
		/// Returns instances of all children of parent type
		/// </summary>
		public static List<TParent> GetChildren<TParent> () {
			return (from a in AppDomain.CurrentDomain.GetAssemblies ()
					from t in a.GetTypes ()
					where t.IsSubclassOf (typeof (TParent))
					select (TParent) Activator.CreateInstance (t)).ToList ();
		}

		/// <summary>
		/// Returns instances of all children of parent type
		/// </summary>
		public static string[] GetChildrenStrings<TParent> () {
			return (from a in AppDomain.CurrentDomain.GetAssemblies ()
					from t in a.GetTypes ()
					where t.IsSubclassOf (typeof (TParent))
					select t.Name).ToArray ();
		}

		public static Dictionary<TParent, string> GetChildrenDictionary<TParent> () {
			return GetChildren<TParent> ().ToDictionary (t => (TParent) Activator.CreateInstance (t.GetType()), t => t.GetType().Name);
		}

	}
}